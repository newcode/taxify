<?php include 'includes/head.php'; ?>
<div class="tx-page">
    <div class="tx-top"></div>
    <div class="tx-intro tx-wrap">
        <div class="tx-intro__logos">
            <span class="tx-intro__logo tx-intro__logo--citybee"><?php include 'img/citybee-full.svg'; ?></span>
            <span class="tx-intro__and">&</span>
            <span class="tx-intro__logo tx-intro__logo--taxify"><?php include 'img/taxify.svg'; ?></span>
        </div>
        <div class="tx-intro__title">
            <?php include 'img/intro-title.svg'; ?>
        </div>
        <div class="tx-intro__actions">
            <a href="https://registration.citybee.lt/" class="tx-intro__btn tx-btn">Registruokis</a>
        </div>
        <div class="tx-intro__img" style="background-image: url('img/intro.svg');">
            <div class="tx-intro__img-text">
                <b>6 val. = 22€</b>
                <span>160 km <br>įskaičiuota</span>
            </div>
        </div>
        <h2 class="tx-intro__slogan">Imk ir Užsidirbk vairuodamas<br> CityBee automobilį<br> (Hyundai i20, 2016m.)!</h2>
    </div>
    <div class="tx-guide tx-wrap">
        <h3 class="tx-guide__title">Ką reikia padaryti naujam CityBee vartotojui?</h3>
        <ol class="tx-guide__list">
            <li class="tx-guide__item">Registruotis CityBee vartotoju ir pridėti banko kortelę prie savo CityBee paskyros - citybee.lt</li>
            <li class="tx-guide__item">Registruotis Taxify vairuotoju - partners.taxify.eu</li>
            <li class="tx-guide__item">Sulaukti skambučio iš Taxify ir patvirtinimo laiško iš CityBee</li>
            <li class="tx-guide__item">Atvykti į Taxify ofisą registracijos užbaigimui ir programėlės veikimo apmokymui</li>
            <li class="tx-guide__item">Per CityBee programėlę, susirasti laisvą Hyundai i20 automobilį Vilniaus mieste ir jį rezervuoti;</li>
        </ol>
        <div class="tx-guide__text">
            <p>Bzz, Jeigu jau esate registruotas CityBee ir Taxify sistemose, rašykite į <a href="mailto:ruslanas@taxify.eu">ruslanas@taxify.eu</a> dėl naujos paskyros sukūrimo, kuri veiktų būtent su CityBee automobiliais!</p>
        </div>
    </div>
    <div class="tx-offers">
        <div class="tx-offers__offer">
            <div class="tx-wrap">
                <div class="tx-offers__label">Specialus Pasiūlymas</div>
                <div class="tx-offers__title">SPECIALUS PASIŪLYMAS GRUODŽIO MĖNESĮ!</div>
                <ul class="tx-offers__list">
                    <li class="tx-offers__item">CityBee (Hyundai i20) kaina: €22 - 6val. Į kainą įskaičiuoti 160km kuro. Viršijus laiką bus skaičiuojamas +0.7 €/val., viršijus kilometražą + 0.11€/km;</li>
                    <li class="tx-offers__item">Taxify komisinis mokestis: 15% nuo kelionės kainos (vėliau gali kisti)</li>
                </ul>
            </div>
        </div>
        <div class="tx-offers__offer">
            <div class="tx-wrap">
                <div class="tx-offers__label">&nbsp;</br>UŽDARBIS</div>
                <ul class="tx-offers__list">
                    <li class="tx-offers__item">Penktadieniais ir Šeštadieniais <i>€60</i> garantuota! Išbuvus 6 val. prisijungus prie Taxify programėlės ir gavus bent 12 užsakymų, skirtumas yra padengiamas!</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="tx-actions tx-wrap">
        <div class="tx-actions__title">VEIKSMAI PROGRAMĖLĖSE:</div>
        <div class="tx-actions__wrap tx-actions__wrap--citybee">
            <div class="tx-actions__logo tx-actions__logo--citybee">
                <?php include 'img/citybee-full.svg'; ?>
            </div>
            <div class="tx-actions__blocks tx-actions__blocks--citybee tx-clear">
                <div class="tx-actions__block tx-actions__block--citybee">
                    <div class="tx-actions__number">1</div>
                    <div class="tx-actions__text">Įsijunkite <i>CityBee</i> programėlę ir pasirinkite automobilių filtrą apačioje1</div>
                    <div class="tx-actions__img" style="background-image: url('img/citybee1.png')"></div>
                </div>
                <div class="tx-actions__block tx-actions__block--citybee">
                    <div class="tx-actions__number">2</div>
                    <div class="tx-actions__text">Pažymėkite Hyundai i20 automobilius ir ekrano viršuje spustelkite rodyklę atgal</div>
                    <div class="tx-actions__img" style="background-image: url('img/citybee2.png')"></div>
                </div>
                <div class="tx-actions__block tx-actions__block--citybee">
                    <div class="tx-actions__number">3</div>
                    <div class="tx-actions__text">Pradinio laukelio apačioje pasirinkite “Sąrašas” ir matysite visus arčiausiai esančius Hyundai i20 automobilius, išsirinkite Jums tinkančią lokaciją.</div>
                    <div class="tx-actions__img" style="background-image: url('img/citybee3.png')"></div>
                </div>
                <div class="tx-actions__block tx-actions__block--citybee">
                    <div class="tx-actions__number">4</div>
                    <div class="tx-actions__text">Rezervuokite pasirinktą automobilį. Jam atrakinti turėsite 15 min. Rezervavus ilgesniam laikui, po nemokamų 15 min. prasidės mokamas laikas</div>
                    <div class="tx-actions__img" style="background-image: url('img/citybee4.png')"></div>
                </div>
            </div>
            <div class="tx-actions__footnote">Jeigu netyčia užbaigėte kelionę, pradėkite kelionę iš naujo, o apie įvykį informuokite CityBee avilį telefonu +370 700 44844, arba e. paštu info@citybee.lt</div>
        </div>
        <div class="tx-actions__wrap tx-actions__wrap--taxify">
            <div class="tx-actions__logo tx-actions__logo--taxify">
                <?php include 'img/taxify.svg'; ?>
            </div>
            <div class="tx-actions__blocks tx-actions__blocks--taxify tx-clear">
                <div class="tx-actions__block tx-actions__block--taxify">
                    <div class="tx-actions__number tx-actions__number--taxify">1</div>
                    <div class="tx-actions__text tx-actions__text--taxify">Prisijunkite su jums sukurtu Taxify - CityBee prisijungimo vardu ir slaptažodžiu</div>
                    <div class="tx-actions__img tx-actions__img--taxify" style="background-image: url('img/taxify1.png')"></div>
                </div>
                <div class="tx-actions__block tx-actions__block--taxify">
                    <div class="tx-actions__number tx-actions__number--taxify">2</div>
                    <div class="tx-actions__text tx-actions__text--taxify">Paspauskite ant automobilio ikonos, tam kad atsidarytų automobilių pasirinkimų meniu</div>
                    <div class="tx-actions__img tx-actions__img--taxify" style="background-image: url('img/taxify2.png')"></div>
                </div>
                <div class="tx-actions__block tx-actions__block--taxify">
                    <div class="tx-actions__number tx-actions__number--taxify">3</div>
                    <div class="tx-actions__text tx-actions__text--taxify">Išsirinkite automobilio su kuriuo važiuosite valstybinį numerį</div>
                    <div class="tx-actions__img tx-actions__img--taxify" style="background-image: url('img/taxify3.png')"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="tx-footer tx-wrap">
        <div class="tx-footer__title">Bzz, pasirengęs užsidirbti dūgzdamas?</div>
        <div class="tx-footer__footnote">*Šiuo metu tik Vilniuje</div>
        <a class="tx-footer__btn tx-btn" href="https://registration.citybee.lt/">Registruotis</a>
    </div>
</div>
<?php include 'includes/foot.php'; ?>
