<div class="top" data-top>
    <div class="top__stripe">
        <div class="top__logo">
            <?php include 'img/logo.svg';?>
            <a href="" class="top__logo-link"></a>
        </div>
        <div class="top__trigger" data-top-trigger>
            <div class="top__trigger-text">menu</div>
            <div class="top__trigger-text">close</div>
        </div>
    </div>
    <div class="top__nav">
    <div class="top__menu">
        <ul class="top__list">
            <li class="top__item"><a href="" class="top__link">Home</a></li>
            <li class="top__item"><a href="" class="top__link">About us</a></li>
            <li class="top__item"><a href="" class="top__link">News</a></li>
            <li class="top__item"><a href="" class="top__link">Faq</a></li>
            <li class="top__item"><a href="" class="top__link">Investments</a></li>
            <li class="top__item"><a href="" class="top__link">Contacts</a></li>
        </ul>
        <a class="top__btn btn btn--top" href="">
                <span class="btn__img"><?php include 'img/stats.svg';?></span>
            <span class="btn__text">my account</span>
        </a>
    </div>
    </div>
</div>
