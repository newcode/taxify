<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">

<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
    <meta charset="UTF-8">
    <title>CityBee</title>
    <meta name="robots" content="noindex, nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="icon" href="http://www.citybee.lt/wp-content/themes/twentythirteen/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="http://www.citybee.lt/wp-content/themes/twentythirteen/favicon.ico" type="image/x-icon">
    <?php
  $dev = 1;
  if ($dev == 3) {
    require "vendor/less/Less.php";
      $parser = new Less_Parser();
      $parser->parseFile("frontend.less");
      echo '<style type="text/css">'.$parser->getCss().'</style>';
  } else if ($dev == 2) {
    require "vendor/less/Cache.php";
    $name = Less_Cache::Get(array('frontend.less'=>''),array('cache_dir'=>'cache','compress'=>true));
    $compiled = file_get_contents('cache/'.$name);
    echo '<style>'.$compiled.'</style>';
    file_put_contents('frontend.min.css',$compiled);
  } else if ($dev == 1) {
    echo '<link rel="stylesheet" type="text/css" href="frontend.css">';
  } ?>
</head>

<body>
    <div class="tx-app">
