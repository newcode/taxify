'use strict';

// SETUP GLOBAL OBJECT FOR GLOBAL NAMESPACING:
var nc = {};
nc.page = {};
nc.dash = {};


nc.appReady = function() {
    nc.$app = $('.app');
    nc.checkMob();
    $(window).on('resize', function() {
        nc.setMob();
    });
    $(document).on('click', '[data-top-trigger]', function() {
        nc.$app.toggleClass('top-open');
    });
    // $(document).on('click', '*', function(e) {
    //     alert(e.target.classList);
    // });
    if ($('[data-noui]').length) {
        nc.addJS('//cdnjs.cloudflare.com/ajax/libs/noUiSlider/9.0.0/nouislider.min.js', function() {
            nc.initNoui();
        }, 'js/libs/nouislider.min.js');
    }
    if ($('[data-swiper]').length) {
        nc.addJS('//cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.0/js/swiper.js', function() {
            nc.initSwipers();
        }, 'js/libs/swiper.min.js');
    }
    if ($('[data-counter]').length) {
        nc.addJS('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment.min.js', function() {
            nc.targetDate = $('[data-counter-date]').attr('data-counter-date');
            nc.initCounters();
        }, 'js/libs/moment.min.js');
    }
    nc.isTop = nc.$app.hasClass('is-top');
    $(window).on('scroll', function(e) {
        nc.scrollPos = this.scrollY;
        if (nc.isTop && nc.scrollPos > 2) {
            nc.isTop = false;
            nc.$app.removeClass('is-top');
            console.log('switch');
        } else if (!nc.isTop && nc.scrollPos < 3) {
            nc.isTop = true;
            nc.$app.addClass('is-top');
            console.log('switch');
        }
    });
}

nc.initCounters = function() {
    nc.daysTo = moment(nc.targetDate).diff(moment(), 'days');
    nc.hoursTo = moment(nc.targetDate).diff(moment(), 'hours') - (nc.daysTo * 24);
    nc.minutesTo = moment(nc.targetDate).diff(moment(), 'minutes') - (nc.daysTo * 24 * 60) - (nc.hoursTo * 60);
    $('[data-counter]').each(function() {
        var type = $(this).attr('data-counter');
        $(this).text(nc[type + 'To']);
        $(this).addClass('is-ready');

    });
    setTimeout(function() {
        nc.initCounters();
    }, 60000);
}

nc.initNoui = function() {
    $('[data-noui]').each(function() {
        var title = $(this).attr('data-noui'),
            $target = $(this).find('[data-noui-target]')[0],
            $current = $(this).find('[data-noui-current]'),
            minVal = Number($(this).find('[data-noui-min]').text()),
            maxVal = Number($(this).find('[data-noui-max]').text()),
            $obj = $('[data-calc-ratio]'),
            ratio = Number($obj.attr('data-calc-ratio'));
        nc.page[title] = Number($current.val());
        noUiSlider.create($target, {
            start: [nc.page[title]],
    connect: [true, false],
            range: {
                'min': [minVal],
                'max': [maxVal]
            },
            step: minVal
        }).on('update', function(values, handle) {
            nc.page[title] = Math.round(values[0]);
            $current.val(nc.page[title]);
            $obj.val(ratio * nc.page.amount * nc.page.time);
        });
    });
};

nc.initSwipers = function() {
    $('[data-swiper]').each(function() {
        var title = $(this).attr('data-swiper'),
            $target = $(this).find('[data-swiper-target]'),
            $bullets = $(this).find('[data-swiper-bullets]'),
            $this = $(this);
        nc[title + 'Swiper'] = new Swiper($target, {
            effect: 'fade',
            fade: {
                crossFade: true
            },
            loop: true,
            pagination: $bullets,
            bulletActiveClass: 'is-active',
            bulletClass: 'intro__bullet',
            simulateTouch: false,
            // paginationClickable: true,
            autoplay: 2500,
            paginationBulletRender: function(swiper, index, className) {
                return '<div class="' + className + '" data-swiper-bullet></div>';
            },
            onInit: function() {
                $this.addClass('is-loaded');
            }
        });
    });
}

nc.checkMob = function() {
    nc.mobSet = false;
    nc.winW = window.innerWidth;
    nc.winH = window.innerHeight;
    nc.bodyH = document.body.clientHeight;
    nc.bodyW = document.body.clientWidth;
    nc.isTouch = typeof window.ontouchstart !== "undefined";
    if (768 > nc.winW && !nc.isMob && !nc.$app.hasClass('is-mob')) {
        nc.$app.addClass('is-mob');
    } else if (768 <= nc.winW && nc.isMob && nc.$app.hasClass('is-mob')) {
        nc.$app.removeClass('is-mob');
    }
    if (nc.isTouch && !nc.$app.hasClass('is-touch')) {
        nc.$app.addClass('is-touch');
        nc.$app.removeClass('no-touch');
    } else if (!nc.isTouch && !nc.$app.hasClass('no-touch')) {
        nc.$app.addClass('no-touch');
        nc.$app.removeClass('is-touch');
    }
    nc.isMob = 768 > nc.winW;
};

nc.setMob = function() {
    if (!nc.mobSet) {
        nc.mobSet = true;
        setTimeout(function() {
            nc.checkMob();
        }, 100);
    }
};

nc.addJS = function(url, callback, fallback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    if (script.readyState) {
        //IE
        script.onreadystatechange = function() {
            if (script.readyState === "loaded" || script.readyState === "complete") {
                script.onreadystatechange = null;
                callback();
            } else if (script.readyState === "error") {
                nc.addJS(fallback, callback, function() {
                    callback();
                });
            }
        };
    } else {
        //Others
        script.onload = function() {
            callback();
        };
        script.onerror = function() {
            nc.addJS(fallback, callback, function() {
                callback();
            });
        }
    }
    document.getElementsByTagName("head")[0].appendChild(script);
};

nc.addJS('//code.jquery.com/jquery-1.11.3.min.js', function() {
    $(function() {
        nc.appReady();
    });
}, 'js/libs/jquery-1.11.3.min.js');
